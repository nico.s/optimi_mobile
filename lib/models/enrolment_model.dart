class Enrolment {
  int groupId;
  String sId;
  String code;
  String grade;
  String name;
  bool private;
  int updated;
  int created;
  int year;
  String language;
  String source;
  Null root;
  String metadata;
  bool deleted;
  Null deletedDate;
  int owner;
  int updatedBy;
  Null deletedBy;
  String subject;
  int start;
  int end;
  bool systemGroup;
  Null memberParents;
  bool educatorControl;
  int subjectId;

  Enrolment(
      {this.groupId,
      this.sId,
      this.code,
      this.grade,
      this.name,
      this.private,
      this.updated,
      this.created,
      this.year,
      this.language,
      this.source,
      this.root,
      this.metadata,
      this.deleted,
      this.deletedDate,
      this.owner,
      this.updatedBy,
      this.deletedBy,
      this.subject,
      this.start,
      this.end,
      this.systemGroup,
      this.memberParents,
      this.educatorControl,
      this.subjectId});

  factory Enrolment.fromJson(Map<String, dynamic> json) {
    return Enrolment(
      groupId: json['group_id'],
      sId: json['_id'],
      code: json['code'],
      grade: json['grade'],
      name: json['name'],
      private: json['private'],
      updated: json['updated'],
      created: json['created'],
      year: json['year'],
      language: json['language'],
      source: json['source'],
      root: json['root'],
      metadata: json['metadata'],
      deleted: json['deleted'],
      deletedDate: json['deletedDate'],
      owner: json['owner'],
      updatedBy: json['updatedBy'],
      deletedBy: json['deletedBy'],
      subject: json['subject'],
      start: json['start'],
      end: json['end'],
      systemGroup: json['systemGroup'],
      memberParents: json['memberParents'],
      educatorControl: json['educatorControl'],
      subjectId: json['subject_id'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['group_id'] = this.groupId;
    data['_id'] = this.sId;
    data['code'] = this.code;
    data['grade'] = this.grade;
    data['name'] = this.name;
    data['private'] = this.private;
    data['updated'] = this.updated;
    data['created'] = this.created;
    data['year'] = this.year;
    data['language'] = this.language;
    data['source'] = this.source;
    data['root'] = this.root;
    data['metadata'] = this.metadata;
    data['deleted'] = this.deleted;
    data['deletedDate'] = this.deletedDate;
    data['owner'] = this.owner;
    data['updatedBy'] = this.updatedBy;
    data['deletedBy'] = this.deletedBy;
    data['subject'] = this.subject;
    data['start'] = this.start;
    data['end'] = this.end;
    data['systemGroup'] = this.systemGroup;
    data['memberParents'] = this.memberParents;
    data['educatorControl'] = this.educatorControl;
    data['subject_id'] = this.subjectId;
    return data;
  }
}
