import 'dart:convert';

class User {
  int userId;
  String id;
  String username;
  String firstname;
  String surname;
  bool enabled;
  String email;
  String hash;

  User({this.userId, this.id, this.username, this.firstname, this.surname, this.enabled, this.email, this.hash});

  factory User.fromReqBody(String body) {
    Map<String, dynamic> json = jsonDecode(body);

    return User(
      userId: json['user']['user_id'],
      id: json['user']['_id'],
      username: json['user']['username'],
      firstname: json['user']['firstname'],
      surname: json['user']['surname'],
      enabled: json['user']['enabled'],
      email: json['user']['email'],
      hash: json['hash']
    );
  }

  void printAttributes() {
    print("user_id: ${this.userId}\n");
    print("_id: ${this.id}\n");
    print("username: ${this.username}\n");
    print("firstname: ${this.firstname}\n");
    print("surname: ${this.surname}\n");
    print("enabled: ${this.enabled}\n");
    print("email: ${this.email}\n");
    print("hash: ${this.hash}\n");
  }
}
