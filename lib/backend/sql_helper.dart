import 'dart:io';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class SQLHelper {

  static final _databaseName = "MyDatabase.db";
  static final _databaseVersion = 1;

  static final table = 'my_table';

  static final columnId = '_id';
  static final columnUsername = 'username';
  static final columnFirstname = 'firstname';
  static final columnSurname = 'surname';
  static final columnEmail = 'email';
  static final columnHash = 'hash';

  SQLHelper._privateConstructor();
  static final SQLHelper instance = SQLHelper._privateConstructor();

  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    // lazily instantiate the db the first time it is accessed
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion,
        onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $table (
            $columnId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            $columnUsername TEXT NOT NULL,
            $columnFirstname TEXT NOT NULL,
            $columnSurname TEXT NOT NULL,
            $columnEmail TEXT,
            $columnHash TEXT NOT NULL
            
          )
          ''');
  }

  Future<int> insert(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(table, row);
  }

  Future<List<Map<String, dynamic>>> queryAllRows() async {
    Database db = await instance.database;
    return await db.query(table);
  }

 Future<List<Map<String, dynamic>>> getUser(int id) async {
  Database db = await instance.database;
  return db.query(table, where: "_id = ?", whereArgs: [id], limit: 1);
}
  
  Future<List<Map<String, dynamic>>> getLast() async {
    Database db = await instance.database;
    return db.rawQuery('select last_insert_rowid()');
  }


}