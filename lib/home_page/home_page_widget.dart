import 'dart:convert';

import 'package:mobile_a_p_i/core/auth_api.dart';
import 'package:mobile_a_p_i/models/enrolment_model.dart';
import 'package:mobile_a_p_i/models/user_model.dart';

import '../backend/sql_helper.dart';
import '../flutter_flow/flutter_flow_theme.dart';
import '../flutter_flow/flutter_flow_util.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../models/enrolment_model.dart';

Future<List<Enrolment>> view() async {
  AuthApi authApi = AuthApi();
  final dbHelper = SQLHelper.instance;

  try {
    final lastRow = await dbHelper.getLast();
    // print('Last entry:  ${lastRow[0]['last_insert_rowid()']}');
    final firstRow = await dbHelper.getUser(lastRow[0]['last_insert_rowid()']);
    // print('Doing API request with username:  ${firstRow[0]['username']}');
    // print('Doing API request with hash:  ${firstRow[0]['hash']}');
    final res = await authApi.getEnrolment(
        firstRow[0]['username'], firstRow[0]['hash']);
    // print('Doing API request:  ${res.body}');

    List jsonResponse = json.decode(res.body);

    return jsonResponse.map((data) => new Enrolment.fromJson(data)).toList();
  } catch (e) {
    print(e);
  }
}

class HomePageWidget extends StatefulWidget {
  const HomePageWidget({Key key}) : super(key: key);

  @override
  _HomePageWidgetState createState() => _HomePageWidgetState();
}

class _HomePageWidgetState extends State<HomePageWidget> {
  Future<List<Enrolment>> futureData = view();

  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final username = ModalRoute.of(context).settings.arguments;

    return MaterialApp(
      title: 'Optimi',
      home: Scaffold(
          appBar: AppBar(
            title: Text(
              username.toString() + '\'s Groups',
            ),
          ),
          body: Center(
            child: FutureBuilder<List<Enrolment>>(
                future: futureData,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<Enrolment> data = snapshot.data;
                    return ListView.builder(
                        itemCount: data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            height: 75,
                            color: Colors.white,
                            child: Center(
                              child: Text(data[index].name),
                            ),
                          );
                        });
                  } else if (snapshot.hasError) {
                    return Text("${snapshot.error}");
                  }
                  return CircularProgressIndicator();
                }),
          )),
    );
  }
}
