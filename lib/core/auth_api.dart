import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mobile_a_p_i/core/base_api.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class AuthApi extends BaseAPI {

  Future<http.Response> login(String username, String password) async {
    var body = jsonEncode({
      'username': username,
      'password': password,
      'noHash': 0
    });

    http.Response response = await http.post(Uri.parse(super.loginPath), headers: super.headers, body: body);

    return response;
  }

  Future<http.Response> reloadLogin(String username, String password) async {
    final headers = {
      "Content-Type": "application/x-www-form-urlencoded"
    };
    var body = {
      'username': username,
      'password': password,
      'noHash': '1'
    };
    print(body);

    http.Response response = await http.post(Uri.parse(super.loginPath), headers: headers, encoding: Encoding.getByName('utf-8') ,body: body);
    print(response);

    return response;
  }

  Future<http.Response> getEnrolment(String _username, String token) async {

    var username = _username;
    var url = 'https://app.dev.it.si/unity/api/1.0/users/$username/enrolment';

    // print(url);
    // Map<String, String> headers = {
    //   'Content-Type': 'application/json; charset=utf-8',
    //   'Accept': 'application/json',
    //   'Authorization': 'Bearer $token',
    // };

    print('Token : ${token}');

    String username1 = 'assignment';
    String password = '1';

    String auth = 'Basic ' + base64.encode(utf8.encode('$username1:$password'));
    print('final URL $url');
    print('final header $auth');

    final response = await http.get(
      Uri.parse(url),
      headers: {
        HttpHeaders.authorizationHeader: auth
      }
    );

    print('final response $response');
    return response;
  }


}